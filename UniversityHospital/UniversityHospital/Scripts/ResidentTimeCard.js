﻿//Display the username
$(function() {
    var username = lib.getParam("user");
    //since this was user entered, 
    $('#username').text(username);
getTimes();
    
});
function addTime() {
    var notifyText = "added";
    $('#timeEntryNotify').attr('class', 'alert alert-success');

    var calendarWeek = $('#dutystartweek').val();
    var dutyHours = $('#dutyhours').val();
   if (calendarWeek === "" || calendarWeek < 1 || calendarWeek > 53) {
       notifyText = "You have not entered a valid calendar week. ";
   }
   if (dutyHours === "" || dutyHours < 1 || dutyHours > 24) {
       notifyText = " Those are some crazy hours worked. ";
   }
    if (notifyText !== "added") {
        notifyText = "Oops! " + notifyText + "Please correct then try again.";
        $('#timeEntryNotify').attr('class', 'alert alert-danger');
    } else {
        lib.saveForm("TimeEntry", "TimeEntry_" + lib.getParam("user"));
    }
    $('#timeEntryNotify').text(notifyText);
}
    
    function getTimes() {
        var localSaves = lib.localGet("TimeEntry_" + lib.getParam("user"));
        $('#enteredTime').text(localSaves);
    }